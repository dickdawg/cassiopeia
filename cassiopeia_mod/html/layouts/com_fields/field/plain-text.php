<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_fields
 *
 * @copyright   (C) 2016 Open Source Matters, Inc. <https://www.joomla.org>
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

use Joomla\CMS\Language\Text;

if (!array_key_exists('field', $displayData))
{
	return;
}

$field = $displayData['field'];
$value = $field->value;
$valueClass = $field->params->get('value_render_class');

if ($value == '')
{
	return;
}

?>
<span class="field-value <?php echo $valueClass; ?>"> <?php echo $value; ?></span>