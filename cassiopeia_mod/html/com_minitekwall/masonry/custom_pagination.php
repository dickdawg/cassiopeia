<?php

/**
 * @title        Minitek Wall
 * @copyright    Copyright (C) 2011-2022 Minitek, All rights reserved.
 * @license      GNU General Public License version 3 or later.
 * @author url   https://www.minitek.gr/
 * @developers   Minitek.gr
 */

defined('_JEXEC') or die;

use Joomla\CMS\HTML\HTMLHelper;

?><div class="mwall-pagination"><?php
	// Load more
	if (isset($this->pagination) && $this->pagination == '1') 
	{
		?><div class="mwall-append"><?php 
			if ($this->remainingCount > 0)
			{
				?><a href="#" class="mwall-button" data-page="<?php echo $this->next_page; ?>">
					<span class="mwall-more-results"><?php
						echo \JText::_('COM_MINITEKWALL_LOAD_MORE_ITEMS');

						if ($this->showRemaining) 
						{
							?> (<span class="mwall-total-items"><?php 
								if (isset($this->remainingCount))
									echo $this->remainingCount; 
							?></span>)<?php
						}
					?></span>
					<div class="mwall-append-loader mwall-loader"> </div>
				</a><?php
			}

			if ($this->mas_pag_reset_filters) 
			{
				?><a href="#" class="mwall-reset-btn">
						<span><?php echo \JText::_('COM_MINITEKWALL_RESET_FILTERS'); ?></span>
				</a><?php
			}
		?></div><?php
	}
	// Infinite
	else if (isset($this->pagination) && $this->pagination == '4') 
	{
		?><div class="mwall-append mwall-infinite" data-last-page="<?php echo $this->last_page; ?>">
			<div class="mwall-more-results" data-page="2">
				<div class="mwall-append-loader mwall-loader"> </div>
			</div>
		</div><?php
	}
	// Arrows
	else if (isset($this->pagination) && $this->pagination == '2') 
	{
		?><div class="mwall-arrows" data-last-page="<?php echo $this->last_page; ?>"><?php
			if ($this->remainingCount > 0)
			{
				?><a href="#" class="mwall-arrow mwall-arrow-prev disabled" data-page="<?php echo $this->previous_page; ?>" title="<?php echo \JText::_('COM_MINITEKWALL_PREVIOUS_PAGE'); ?>">
					<span class="mwall-more-results"><?php
						if ($this->arrows !== 'text') 
						{
							?><i class="fa fa-<?php echo $this->arrows; ?>-left"></i><?php
						} 
						else 
							echo \JText::_('COM_MINITEKWALL_PREVIOUS');
					?></span>
					<div class="mwall-arrow-loader mwall-loader"> </div>
				</a><?php

				$next_class = '';

				if ($this->totalPages == 1) 
				{
					$next_class = 'disabled';
				}

				?><a href="#" class="mwall-arrow mwall-arrow-next <?php echo $next_class; ?>" data-page="<?php echo $this->next_page; ?>" title="<?php echo \JText::_('COM_MINITEKWALL_NEXT_PAGE'); ?>">
					<span class="mwall-more-results"><?php
						if ($this->arrows !== 'text') 
						{
							?><i class="fa fa-<?php echo $this->arrows; ?>-right"></i><?php
						} 
						else 
							echo \JText::_('COM_MINITEKWALL_NEXT');
					?></span>
					<div class="mwall-arrow-loader mwall-loader"> </div>
				</a><?php
			}

			if ($this->mas_pag_reset_filters) 
			{
				?><a href="#" class="mwall-reset-btn">
						<span><?php echo \JText::_('COM_MINITEKWALL_RESET_FILTERS'); ?></span>
				</a><?php
			}
		?></div><?php
	}
	// Pages
	else if (isset($this->pagination) && $this->pagination == '3') 
	{
		?><div class="mwall-pages"><?php
			if ($this->remainingCount > 0)
			{
				for ($i = 1; $i <= (int)$this->totalPages; $i++) 
				{
					if ($i == 1) 
						$active_page = 'mwall-active';
					else 
						$active_page = '';

					?><a href="#" class="mwall-page <?php echo $active_page; ?>" data-page="<?php echo $i; ?>">
						<span class="page-number"><?php echo $i; ?></span>
						<div class="mwall-page-loader mwall-loader"> </div>
					</a><?php
				}
			}

			if ($this->mas_pag_reset_filters) 
			{
				?><a href="#" class="mwall-reset-btn">
					<span><?php echo \JText::_('COM_MINITEKWALL_RESET_FILTERS'); ?></span>
				</a><?php
			}
		?></div><?php
	}
?></div><?php