<?php
/***          [ Dog-Shit.net ]           ***/
/***       Featured Articles Layout      ***/
/*** Displays each article as Intro Card ***/


defined('_JEXEC') or die;

use Joomla\CMS\Factory;
use Joomla\CMS\Language\Associations;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Layout\LayoutHelper;
use Joomla\CMS\Router\Route;
use Joomla\CMS\Uri\Uri;
use Joomla\Component\Content\Administrator\Extension\ContentComponent;
use Joomla\Component\Content\Site\Helper\RouteHelper;
$params  = &$this->item->params;
$canEdit = $this->item->params->get('access-edit');
$info    = $this->item->params->get('info_block_position', 0);
$assocParam = (Associations::isEnabled() && $params->get('show_associations'));
$currentDate       = Factory::getDate()->format('Y-m-d H:i:s');
$isExpired         = !is_null($this->item->publish_down) && $this->item->publish_down < $currentDate;
$isNotPublishedYet = $this->item->publish_up > $currentDate;
$isUnpublished     = $this->item->state == ContentComponent::CONDITION_UNPUBLISHED || $isNotPublishedYet || $isExpired;
?>
<!-- *** RENDER CUSTOM FIELDS -->
<?php foreach($item->jcfields as $jcfield)
{
$item->jcFields[$jcfield->id] = $jcfield;
}
?>
<!-- *** Article Card hover effect *** -->
<script>
$(window).load(function() {
$('.post-module').hover(function() {
$(this).find('.description').stop().animate({
height: "toggle",
opacity: "toggle"
}, 300);
});
});
</script>

<!-- #2 *** Article Card -->
	<div class="item-content post-module">
	
<!-- !!! DO NOT EDIT !!! -->
	<?php if ($isUnpublished) : ?>
		<div class="system-unpublished">
	<?php endif; ?>
	
<!-- #3 *** Card Thumbnail-->
		<div class="thumbnail">
			<div class="date">
			
<!-- / #4 *** Load Article Info -->
			<?php $useDefList = ($params->get('show_modify_date') || $params->get('show_publish_date') || $params->get('show_create_date')
			|| $params->get('show_hits') || $params->get('show_category') || $params->get('show_parent_category') || $params->get('show_author') || $assocParam); ?>
			<?php if ($useDefList && ($info == 0 || $info == 2)) : ?>
			
			<?php echo LayoutHelper::render('joomla.content.info_block', array('item' => $this->item, 'params' => $params, 'position' => 'above')); ?>
			<?php endif; ?>
			</div>
<!-- \ #5 image RENDER -->
			<?php echo LayoutHelper::render('joomla.content.intro_image', $this->item); ?>
		</div>
	


<!-- *** Post Content *** -->
			<div class="post-content">
			
				<div class="category">
					<?php if (isset($this->item->jcfields[3])): ?>
					<?php echo FieldsHelper::render('com_content.article', 'field.plain-text', array('field'=> $this->item->jcfields[3])); ?>
					<?php endif; ?>
				</div>
				
		<?php if ($info == 0 && $params->get('show_tags', 1) && !empty($this->item->tags->itemTags)) : ?>
			<?php echo LayoutHelper::render('joomla.content.tags', $this->item->tags->itemTags); ?>
		<?php endif; ?>


	<?php if ($params->get('show_title')) : ?>
		<h2 class="title item-title liner" itemprop="headline">
		<?php if ($params->get('link_titles') && $params->get('access-view')) : ?>
			<a href="<?php echo Route::_(RouteHelper::getArticleRoute($this->item->slug, $this->item->catid, $this->item->language)); ?>" itemprop="url">
				<?php echo $this->escape($this->item->title); ?>
			</a>
		<?php else : ?>
			<?php echo $this->escape($this->item->title); ?>
		<?php endif; ?>
		</h2>
	<?php endif; ?>

<!-- *** IGNORE *** -->
	<?php if ($this->item->state == ContentComponent::CONDITION_UNPUBLISHED) : ?>
		<span class="badge bg-warning text-light"><?php echo Text::_('JUNPUBLISHED'); ?></span>
	<?php endif; ?>

	<?php if ($isNotPublishedYet) : ?>
		<span class="badge bg-warning text-light"><?php echo Text::_('JNOTPUBLISHEDYET'); ?></span>
	<?php endif; ?>
	<?php if ($isExpired) : ?>
		<span class="badge bg-warning text-light"><?php echo Text::_('JEXPIRED'); ?></span>
	<?php endif; ?>
	<?php if ($canEdit) : ?>
		<?php echo LayoutHelper::render('joomla.content.icons', array('params' => $params, 'item' => $this->item)); ?>
	<?php endif; ?>


<!--	<?php // Content is generated by content plugin event "onContentAfterTitle" ?> -->
	<?php echo $this->item->event->afterDisplayTitle; ?>

<!--	<?php // @todo Not that elegant would be nice to group the params ?> -->


	<?php // Content is generated by content plugin event "onContentBeforeDisplay" ?>
	<?php echo $this->item->event->beforeDisplayContent; ?>

<!-- Custom article Card Wrapper -->
<div class="description">
	<?php echo $this->item->introtext; ?>
</div>

<!-- Custom Card Footer -->
<div class="post-meta">
	<?php if ($info == 1 || $info == 2) : ?>
		<?php if ($useDefList) : ?>
			<?php echo LayoutHelper::render('joomla.content.info_block', array('item' => $this->item, 'params' => $params, 'position' => 'below')); ?>
		<?php endif; ?>
		<?php if ($params->get('show_tags', 1) && !empty($this->item->tags->itemTags)) : ?>
			<?php echo LayoutHelper::render('joomla.content.tags', $this->item->tags->itemTags); ?>
		<?php endif; ?>
	<?php endif; ?>

	<!-- Render Footer Fields -->
	<?php if (isset($this->item->jcfields[4])): ?>
		<span class="badge bg-secondary"><svg enable-background="new 0 0 100 100" width="0.3in" height="0.3in" viewBox="0 0 100 100" xmlns="http://www.w3.org/2000/svg"><path d="m20.5 82.1h12.1v-12h-12.1zm14.8 0h13.4v-12h-13.4zm-14.8-14.7h12.1v-13.4h-12.1zm14.8 0h13.4v-13.4h-13.4zm-14.8-16.1h12.1v-12h-12.1zm30.8 30.8h13.4v-12h-13.4zm-16-30.8h13.4v-12h-13.4zm32.1 30.8h12.1v-12h-12.1zm-16.1-14.7h13.4v-13.4h-13.4zm-14.7-36.2v-12c0-.4-.1-.7-.4-.9-.3-.3-.6-.4-.9-.4h-2.7c-.4 0-.7.1-.9.4s-.4.6-.4.9v12.1c0 .4.1.7.4.9.3.3.6.4.9.4h2.7c.4 0 .7-.1.9-.4.3-.3.4-.6.4-1zm30.8 36.2h12.1v-13.4h-12.1zm-16.1-16.1h13.4v-12h-13.4zm16.1 0h12.1v-12h-12.1zm1.4-20.1v-12c0-.4-.1-.7-.4-.9s-.6-.4-.9-.4h-2.7c-.4 0-.7.1-.9.4s-.4.6-.4.9v12.1c0 .4.1.7.4.9.3.3.6.4.9.4h2.7c.4 0 .7-.1.9-.4s.4-.6.4-1zm16-2.6v53.6c0 1.5-.5 2.7-1.6 3.8s-2.3 1.6-3.8 1.6h-58.9c-1.5 0-2.7-.5-3.8-1.6s-1.6-2.3-1.6-3.8v-53.6c0-1.5.5-2.7 1.6-3.8s2.3-1.6 3.8-1.6h5.4v-4c0-1.8.7-3.4 2-4.7s2.9-2 4.7-2h2.7c1.8 0 3.4.7 4.7 2s2 2.9 2 4.7v4h16v-4c0-1.8.7-3.4 2-4.7s2.9-2 4.7-2h2.7c1.8 0 3.4.7 4.7 2s2 2.9 2 4.7v4h5.4c1.5 0 2.7.5 3.8 1.6s1.5 2.3 1.5 3.8z" fill="#CCB399"/></svg>
			<?php echo FieldsHelper::render('com_content.article', 'field.render', array('field'=> $this->item->jcfields[4])); ?>
		</span>
	<?php endif; ?>
					
	<?php if (isset($this->item->jcfields[3])): ?>
		<span class="badge bg-secondary"><svg enable-background="new 0 0 100 100" width="0.3in" height="0.3in" viewBox="0 0 100 100" width="100" xmlns="http://www.w3.org/2000/svg"><g fill="#CCB399"><circle cx="49.244" cy="20.203" r="8.2"/><path d="m72.147 54.101-5.47-20.414c-.003-.016-.009-.031-.013-.047l-.096-.359-.014.004c-.499-1.556-1.94-2.69-3.662-2.69-.013 0-.026.004-.039.004v-.071h-25.745v.067c-1.722 0-3.163 1.134-3.662 2.69l-.014-.004-.096.358c-.004.017-.01.032-.013.049l-5.47 20.413.052.014c-.025.169-.052.337-.052.513 0 1.924 1.56 3.485 3.485 3.485 1.474 0 2.727-.919 3.236-2.212l.024.006 4.013-14.974v.057h.031c.11-.493.529-.869 1.054-.869.526 0 .945.377 1.055.869h.033v.161c.002.025.014.046.014.071s-.013.046-.014.071v.153l-6.424 23.978c-.104.211-.167.442-.184.688l-.014.051.008.002c-.001.027-.008.053-.008.081 0 1.033.838 1.87 1.871 1.87h4.751v15.849c0 2.226 1.805 4.032 4.032 4.032s4.032-1.805 4.032-4.032v-15.849h2.293v15.849c0 2.226 1.806 4.032 4.033 4.032s4.032-1.805 4.032-4.032v-15.849h4.765c1.033 0 1.871-.837 1.871-1.87 0-.028-.007-.054-.008-.081l.008-.002-.014-.051c-.018-.246-.081-.477-.184-.688l-6.404-23.904.021-.006c-.027-.095-.059-.188-.059-.291 0-.608.493-1.102 1.102-1.102.518 0 .932.365 1.05.847l.042-.011 4.006 14.951.024-.006c.509 1.293 1.762 2.212 3.236 2.212 1.925 0 3.485-1.561 3.485-3.485 0-.176-.027-.344-.052-.513z"/></g></svg>
			<?php echo FieldsHelper::render('com_content.article', 'field.render', array('field'=> $this->item->jcfields[3])); ?>
		</span>
	<?php endif; ?>		

<!-- Read More -->					
	<?php if ($params->get('show_readmore') && $this->item->readmore) :
		if ($params->get('access-view')) :
			$link = Route::_(RouteHelper::getArticleRoute($this->item->slug, $this->item->catid, $this->item->language));
		else :
			$menu = Factory::getApplication()->getMenu();
			$active = $menu->getActive();
			$itemId = $active->id;
			$link = new Uri(Route::_('index.php?option=com_users&view=login&Itemid=' . $itemId, false));
			$link->setVar('return', base64_encode(RouteHelper::getArticleRoute($this->item->slug, $this->item->catid, $this->item->language)));
		endif; ?>
	<!-- Render Readmore -->
		<?php echo LayoutHelper::render('joomla.content.readmore', array('item' => $this->item, 'params' => $params, 'link' => $link)); ?>

	<?php endif; ?>
</div>
</div>
	<?php if ($isUnpublished) : ?>
		</div>
	<?php endif; ?>

</div>

<?php // Content is generated by content plugin event "onContentAfterDisplay" ?>
<?php echo $this->item->event->afterDisplayContent; ?>
