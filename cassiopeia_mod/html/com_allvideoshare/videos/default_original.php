<?php
/**
 * @version    4.1.0
 * @package    Com_AllVideoShare
 * @author     Vinoth Kumar <admin@mrvinoth.com>
 * @copyright  Copyright (c) 2012 - 2022 Vinoth Kumar. All Rights Reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined( '_JEXEC' ) or die;

use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Factory;
use Joomla\CMS\Router\Route;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Uri\Uri;
use MrVinoth\Component\AllVideoShare\Site\Helper\AllVideoShareHelper;
use MrVinoth\Component\AllVideoShare\Site\Helper\AllVideoShareRoute;

HTMLHelper::_( 'bootstrap.tooltip' );

$app = Factory::getApplication();

// Vars
$player_ratio = $this->params->get( 'player_ratio', 56.25 );
$image_ratio  = $this->params->get( 'image_ratio', 56.25 );

$columns = (int) $this->params->get( 'cols', 3 );
$column_no = floor( 12 / $columns );

$column_class = 'col-md-' . $column_no;
if ( $column_no > 2 ) $column_class .= ' col-sm-6 col-6';

$popup_class  = $this->params->get( 'popup' ) ? ' avs-popup' : '';

// Import CSS
$wa = $app->getDocument()->getWebAssetManager();

if ( $this->params->get( 'load_bootstrap' ) ) {
	$wa->useStyle( 'com_allvideoshare.bootstrap' );
}

if ( $this->params->get( 'popup' ) ) {
	$wa->useStyle( 'com_allvideoshare.popup' )
		->useScript( 'com_allvideoshare.popup' )
		->useScript( 'com_allvideoshare.site' );
}

$wa->useStyle( 'com_allvideoshare.site' );

if ( $css = $this->params->get( 'custom_css' ) ) {
    $wa->addInlineStyle( $css );
}
?>

<div id="avs-videos" class="avs videos">
	<?php if ( $this->params->get( 'show_page_heading' ) ) : ?>
		<div class="page-header">
			<h1>
				<?php if ( $this->escape( $this->params->get( 'page_heading' ) ) ) : ?>
					<?php echo $this->escape( $this->params->get( 'page_heading' ) ); ?>
				<?php else : ?>
					<?php echo $this->escape( $this->params->get( 'page_title' ) ); ?>
				<?php endif; ?>

				<?php if ( $this->params->get( 'show_feed' ) ) : ?>
					<a href="<?php echo Route::_( 'index.php?option=com_allvideoshare&view=videos&format=feed' ); ?>" class="avs-feed-btn" target="_blank">
						<img src="<?php echo Uri::root(); ?>media/com_allvideoshare/images/rss.png" />
					</a>
				<?php endif; ?>
			</h1>
		</div>
	<?php endif; ?>

	<div class="avs-grid<?php echo $popup_class; ?>" data-player_ratio="<?php echo (float) $player_ratio; ?>">
		<div class="row">
			<?php foreach ( $this->items as $i => $item ) : 
				$route = AllVideoShareRoute::getVideoRoute( $item->slug );
				$item_link = Route::_( $route );

				if ( $this->params->get( 'popup' ) ) {
					$item_link = 'javascript:void(0)';
				}			

				$iframe_src = URI::root() . 'index.php?option=com_allvideoshare&view=player&id=' . $item->id . "&format=raw&autoplay=1";
				?>
				<div class="avs-grid-item avs-video-<?php echo (int) $item->id; ?> <?php echo $column_class; ?>" data-mfp-src="<?php echo $iframe_src; ?>">
					<div class="card mb-3">
						<a href="<?php echo $item_link; ?>" class="avs-responsive-item" style="padding-bottom: <?php echo (float) $image_ratio; ?>%">
							<div class="avs-image" style="background-image: url( '<?php echo AllVideoShareHelper::getImage( $item ); ?>' );">&nbsp;</div>
							
							<svg class="avs-svg-icon avs-svg-icon-play" width="32" height="32" viewBox="0 0 32 32">
								<path d="M16 0c-8.837 0-16 7.163-16 16s7.163 16 16 16 16-7.163 16-16-7.163-16-16-16zM16 29c-7.18 0-13-5.82-13-13s5.82-13 13-13 13 5.82 13 13-5.82 13-13 13zM12 9l12 7-12 7z"></path>
							</svg>
						</a>

						<div class="card-body">
							<div class="avs-title">
								<a href="<?php echo $item_link; ?>" class="card-link"><?php echo $this->escape( $item->title ); ?></a>
							</div>

							<?php if ( $this->params->get( 'excerpt' ) && ! empty( $item->description ) ) : ?>
								<div class="avs-excerpt mt-2">
									<?php echo AllVideoShareHelper::Truncate( $item->description, $this->params->get( 'excerpt_length' ) ); ?>
								</div>
							<?php endif; ?>

							<?php if ( $this->params->get( 'ratings' ) ) : ?>
								<div class="avs-ratings-small mt-1">
									<span class="avs-ratings-stars">
										<span class="avs-ratings-current" style="width: <?php echo (float) $item->ratings; ?>%;"></span>
									</span>
								</div>
							<?php endif; ?> 

							<?php if ( $this->params->get( 'views' ) ) : ?>
								<div class="avs-views-count small mt-1">
									<?php
									echo Text::sprintf(
										'COM_ALLVIDEOSHARE_VIDEOS_VIEWS_COUNT', 
										$item->views
									);
									?>
								</div>
							<?php endif; ?>
						</div>					
					</div>
				</div>
			<?php endforeach; ?>
		</div>

		<?php echo $this->pagination->getListFooter(); ?>
	</div>		
</div>
