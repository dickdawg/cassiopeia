<?php
/**
 * @version    4.1.0
 * @package    Com_AllVideoShare
 * @author     Vinoth Kumar <admin@mrvinoth.com>
 * @copyright  Copyright (c) 2012 - 2022 Vinoth Kumar. All Rights Reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined( '_JEXEC' ) or die;

use Joomla\CMS\Factory;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Router\Route;
use Joomla\CMS\Uri\Uri;
use MrVinoth\Component\AllVideoShare\Site\Helper\AllVideoShareHelper;
use MrVinoth\Component\AllVideoShare\Site\Helper\AllVideoShareRoute;

HTMLHelper::_( 'bootstrap.tooltip' );

// Vars
$image_ratio = $this->params->get( 'image_ratio', 56.25 );

$columns = (int) $this->params->get( 'cols', 3 );
$column_no = floor( 12 / $columns );

$column_class = 'col-md-' . $column_no;
if ( $column_no > 2 ) $column_class .= ' col-sm-6 col-6';

// Import CSS
$wa = Factory::getApplication()->getDocument()->getWebAssetManager();

if ( $this->params->get( 'load_bootstrap' ) ) {
	$wa->useStyle( 'com_allvideoshare.bootstrap' );
}

$wa->useStyle( 'com_allvideoshare.site' );

if ( $css = $this->params->get( 'custom_css' ) ) {
    $wa->addInlineStyle( $css );
}
?>

<div id="avs-categories" class="avs categories">
	<?php if ( $this->params->get( 'show_page_heading' ) ) : ?>
		<div class="page-header">
			<h1>
				<?php if ( $this->escape( $this->params->get( 'page_heading' ) ) ) : ?>
					<?php echo $this->escape( $this->params->get( 'page_heading' ) ); ?>
				<?php else : ?>
					<?php echo $this->escape($this->params->get( 'page_title' ) ); ?>
				<?php endif; ?>

				<?php if ( $this->params->get( 'show_feed' ) ) : ?>
					<a href="<?php echo Route::_( 'index.php?option=com_allvideoshare&view=videos&format=feed' ); ?>" class="avs-feed-btn" target="_blank">
						<img src="<?php echo Uri::root(); ?>media/com_allvideoshare/images/rss.png" />
					</a>
				<?php endif; ?>
			</h1>
		</div>
	<?php endif; ?>

	<div class="avs-grid">
		<div class="row">
			<?php foreach ( $this->items as $i => $item ) : 
				$route = AllVideoShareRoute::getCategoryRoute( $item->slug );
				$item_link = Route::_( $route );
				?>
				<div class="<?php echo $column_class; ?>">
					<div class="card mb-3">
						<a href="<?php echo $item_link; ?>" class="avs-responsive-item" style="padding-bottom: <?php echo $image_ratio; ?>%">
							<div class="avs-image" style="background-image: url( '<?php echo AllVideoShareHelper::getImage( $item ); ?>' );">&nbsp;</div>
						</a>

						<div class="card-body">
							<div class="avs-title">
								<a href="<?php echo $item_link; ?>" class="card-link"><?php echo $this->escape( $item->name ); ?></a>
							</div>								
						</div>					
					</div>
				</div>
			<?php endforeach; ?>
		</div>

		<?php echo $this->pagination->getListFooter(); ?>
	</div>		
</div>
