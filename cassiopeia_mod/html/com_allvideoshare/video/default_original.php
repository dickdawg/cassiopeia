<?php
/**
 * @version    4.1.0
 * @package    Com_AllVideoShare
 * @author     Vinoth Kumar <admin@mrvinoth.com>
 * @copyright  Copyright (c) 2012 - 2022 Vinoth Kumar. All Rights Reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined( '_JEXEC' ) or die;

use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Factory;
use Joomla\CMS\Router\Route;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Uri\Uri;
use MrVinoth\Component\AllVideoShare\Site\Helper\AllVideoShareHelper;
use MrVinoth\Component\AllVideoShare\Site\Helper\AllVideoShareHtml;
use MrVinoth\Component\AllVideoShare\Site\Helper\AllVideoSharePlayer;
use MrVinoth\Component\AllVideoShare\Site\Helper\AllVideoShareRoute;

$app = Factory::getApplication();

// Import CSS
$wa = $app->getDocument()->getWebAssetManager();

if ( $this->params->get( 'load_bootstrap' ) ) {
	$wa->useStyle( 'com_allvideoshare.bootstrap' );
}

if ( $this->params->get( 'popup' ) ) {
	$wa->useStyle( 'com_allvideoshare.popup' )
		->useScript( 'com_allvideoshare.popup' );
}

$wa->useStyle( 'com_allvideoshare.site' );

if ( $css = $this->params->get( 'custom_css' ) ) {
    $wa->addInlineStyle( $css );
}

$wa->useScript( 'com_allvideoshare.site' );

$inlineScript = "
	if ( typeof( avs ) === 'undefined' ) {
		var avs = {};
	};

	avs.baseurl = '" . URI::root() . "';
	avs.userid = " . Factory::getUser()->get( 'id' ) . ";	
	avs.guest_ratings = " . $this->params->get( 'guest_ratings', 0 ) . ";
	avs.guest_likes = " . $this->params->get( 'guest_likes', 0 ) . ";
	avs.message_login_required = '" . Text::_( 'COM_ALLVIDEOSHARE_ALERT_MESSAGE_LOGIN_REQUIRED' ) . "';
";

$wa->addInlineScript( $inlineScript, [ 'position' => 'before' ], [], [ 'com_allvideoshare.site' ] );
?>

<div id="avs-video" class="avs video">
	<?php if ( $this->params->get( 'show_page_heading' ) ) : ?>
		<div class="page-header">
			<h1>
				<?php if ( $this->escape( $this->params->get( 'page_heading' ) ) ) : ?>
					<?php echo $this->escape( $this->params->get( 'page_heading' ) ); ?>
				<?php else : ?>
					<?php echo $this->escape($this->params->get( 'page_title' ) ); ?>
				<?php endif; ?>
			</h1>
		</div>
	<?php endif; ?>
    
    <div class="avs-item">
		<?php if ( $this->params->get( 'search' ) ) :
			$route = AllVideoShareRoute::getSearchRoute(); 
			?>
			<form class="avs-search-form mb-4" action="<?php echo Route::_( $route ); ?>" method="GET" role="search">
				<?php if ( ! AllVideoShareHelper::isSEF() ) : ?>
					<input type="hidden" name="option" value="com_allvideoshare" />
					<input type="hidden" name="view" value="search" />
					<input type="hidden" name="Itemid" value="<?php echo $app->input->getInt( 'Itemid' ); ?>" />
				<?php endif; ?>

				<div class="input-group">
					<input type="text" name="q" class="form-control" placeholder="<?php echo Text::_( 'JSEARCH_FILTER_SUBMIT' ); ?>..." />
					<button class="btn btn-primary" type="submit">
						<span class="icon-search icon-white" aria-hidden="true"></span> <?php echo Text::_( 'JSEARCH_FILTER_SUBMIT' ); ?>
					</button>
				</div>
			</form>
		<?php endif; ?>

		<?php
		// Player
		$args = array( 
			'width'  => $this->params->get( 'player_width' ),
			'ratio'  => $this->params->get( 'player_ratio' ),
			'id'     => $this->item->id,
			'Itemid' => $app->input->getInt( 'Itemid' )
		);

		echo '<div class="mb-4">' . AllVideoSharePlayer::load( $args ) . '</div>';

		if ( $this->params->get( 'ratings' ) || $this->params->get( 'likes' ) ) : ?>
			<?php if ( $this->params->get( 'ratings' ) ) : ?>
				<div id="avs-ratings-widget" class="float-md-start mb-1">
					<?php echo AllVideoShareHtml::RatingsWidget( $this->item, $this->params ); ?>
				</div>
			<?php endif; ?>	
			
			<?php if ( $this->params->get( 'likes' ) ) : ?>
				<div id="avs-likes-dislikes-widget" class="float-md-end mb-1">
					<?php echo AllVideoShareHtml::LikesWidget( $this->item, $this->params ); ?>
				</div>
			<?php endif; ?>

			<div class="clearfix"></div>
		<?php endif;

		// Description	
		if ( $this->params->get( 'description' ) ) {
			echo '<div class="mb-4">' . $this->item->description . '</div>';
		}

		// Meta
		$meta = array();

		if ( $this->params->get( 'category' ) ) { // Categories
			$categories = array();

			$categories[] = sprintf(
				'<a href="%s">%s</a>',
				AllVideoShareRoute::getCategoryRoute( $this->item->category->slug ),
				$this->escape( $this->item->category->name )
			);
			
			if ( $this->canDo && $this->params->get( 'multi_categories' ) && ! empty( $this->item->categories ) ) {
				foreach ( $this->item->categories as $category ) {
					$categories[] = sprintf(
						'<a href="%s">%s</a>',
						AllVideoShareRoute::getCategoryRoute( $category->slug ),
						$this->escape( $category->name )
					);
				}
			}
			
			$meta[] = sprintf(
				'<div class="avs-meta-categories"><span class="icon-folder-open icon-fw"></span> %s: %s</div>',
				( ( count( $categories ) > 1 ) ? Text::_( 'COM_ALLVIDEOSHARE_TITLE_CATEGORIES' ) : Text::_( 'COM_ALLVIDEOSHARE_TITLE_CATEGORY' ) ),
				implode( ', ', $categories )
			);
		}
		          
		if ( $this->params->get( 'views' ) ) { // Views
			$meta[] = sprintf(
				'<div class="avs-meta-views"><span class="icon-eye icon-fw"></span> %s: %s</div>',
				Text::_( 'COM_ALLVIDEOSHARE_TITLE_VIEWS' ),
				$this->item->views
			);
		}	
		
		if ( count( $meta ) ) {
			printf( 
				'<div class="avs-meta mb-4">%s</div>' ,
				implode( '', $meta )
			);
		}
		
		if ( $this->params->get( 'comments_type' ) ) { // Comments
			echo $this->loadTemplate( 'comments' );
		}

		if ( $this->params->get( 'related_videos' ) ) { // Related Videos
			echo $this->loadTemplate( 'related' );
		}
        ?>
    </div>
</div>