<?php
/**
 * @version    4.0.0
 * @package    Com_AllVideoShare
 * @author     Vinoth Kumar <admin@mrvinoth.com>
 * @copyright  Copyright (c) 2012 - 2021 Vinoth Kumar. All Rights Reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined( '_JEXEC' ) or die;

use Joomla\CMS\Factory;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Uri\Uri;
?>

<?php if ( $this->params->get( 'comments_type' ) == 'facebook' ) : ?>
	<div class="avs-comments mb-4">
		<p class="lead"><?php echo Text::_( 'COM_ALLVIDEOSHARE_TITLE_ADD_YOUR_COMMENTS' ); ?></p>

		<div id="fb-root"></div>

		<div class="fb-comments"
			data-href="<?php echo Uri::root() . 'index.php?option=com_allvideoshare&view=video&slg=' . $this->item->slug;; ?>"
			data-numposts="<?php echo $this->params->get( 'comments_posts', 5 ); ?>"
			data-width="100%"
			data-colorscheme="<?php echo $this->params->get( 'comments_color', 'light' ); ?>">
		</div>
	</div>
<?php endif; 