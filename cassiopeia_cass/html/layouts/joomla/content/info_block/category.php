<?php
/**
 * @package     Joomla.Site
 * @subpackage  Layout
 *
 * @copyright   (C) 2013 Open Source Matters, Inc. <https://www.joomla.org>
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use Joomla\CMS\Language\Text;
use Joomla\CMS\Layout\LayoutHelper;
use Joomla\CMS\Router\Route;
use Joomla\Component\Content\Site\Helper\RouteHelper;

?>
       

	<?php $title = $this->escape($displayData['item']->category_title); ?>
	<?php if ($displayData['params']->get('link_category') && !empty($displayData['item']->catid)) : ?>
		<?php $url = '<a href="' . Route::_(
			RouteHelper::getCategoryRoute($displayData['item']->catid, $displayData['item']->category_language)
			)
			. '" itemprop="genre">' . $title . '</a>'; ?>
			<div class="day">
		<?php echo Text::sprintf($url); ?></div>
	<?php else : ?>
	<div class="day">
		<?php echo Text::sprintf('<span itemprop="genre">' . $title . '</span>'); ?></div>
	<?php endif; ?>

	<!-- ICON
	<div class="month">
	<?php echo LayoutHelper::render('joomla.icon.iconclass', ['icon' => 'fa-solid fa-flag-checkered']); ?>
	</div>
	-->
	
