<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_fields
 *
 * @copyright   (C) 2016 Open Source Matters, Inc. <https://www.joomla.org>
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

use Joomla\CMS\Language\Text;

if (!array_key_exists('field', $displayData))
{
	return;
}

$field = $displayData['field'];
$label = Text::_($field->label);
$value = $field->value;
$showLabel = $field->params->get('showlabel');
$prefix = Text::plural($field->params->get('prefix'), $value);
$suffix = Text::plural($field->params->get('suffix'), $value);
$labelClass = $field->params->get('label_render_class');
$valueClass = $field->params->get('value_render_class');

if ($value == '')
{
	return;
}

?>
<div class="tag-card">
  <div class="tag-icon">
    <svg enable-background="new 0 0 100 100" width="50px" height="50px" viewBox="0 0 100 100" width="100" xmlns="http://www.w3.org/2000/svg"><g fill="#CCB399"><circle cx="49.244" cy="20.203" r="8.2"/><path d="m72.147 54.101-5.47-20.414c-.003-.016-.009-.031-.013-.047l-.096-.359-.014.004c-.499-1.556-1.94-2.69-3.662-2.69-.013 0-.026.004-.039.004v-.071h-25.745v.067c-1.722 0-3.163 1.134-3.662 2.69l-.014-.004-.096.358c-.004.017-.01.032-.013.049l-5.47 20.413.052.014c-.025.169-.052.337-.052.513 0 1.924 1.56 3.485 3.485 3.485 1.474 0 2.727-.919 3.236-2.212l.024.006 4.013-14.974v.057h.031c.11-.493.529-.869 1.054-.869.526 0 .945.377 1.055.869h.033v.161c.002.025.014.046.014.071s-.013.046-.014.071v.153l-6.424 23.978c-.104.211-.167.442-.184.688l-.014.051.008.002c-.001.027-.008.053-.008.081 0 1.033.838 1.87 1.871 1.87h4.751v15.849c0 2.226 1.805 4.032 4.032 4.032s4.032-1.805 4.032-4.032v-15.849h2.293v15.849c0 2.226 1.806 4.032 4.033 4.032s4.032-1.805 4.032-4.032v-15.849h4.765c1.033 0 1.871-.837 1.871-1.87 0-.028-.007-.054-.008-.081l.008-.002-.014-.051c-.018-.246-.081-.477-.184-.688l-6.404-23.904.021-.006c-.027-.095-.059-.188-.059-.291 0-.608.493-1.102 1.102-1.102.518 0 .932.365 1.05.847l.042-.011 4.006 14.951.024-.006c.509 1.293 1.762 2.212 3.236 2.212 1.925 0 3.485-1.561 3.485-3.485 0-.176-.027-.344-.052-.513z"/></g></svg>
  </div>
  <div class="tag-text">
<?php echo $value; ?>
</div>
</div>