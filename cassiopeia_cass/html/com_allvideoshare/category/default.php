<?php
/**
 * @version    4.0.0
 * @package    Com_AllVideoShare
 * @author     Vinoth Kumar <admin@mrvinoth.com>
 * @copyright  Copyright (c) 2012 - 2021 Vinoth Kumar. All Rights Reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined( '_JEXEC' ) or die;

use Joomla\CMS\Factory;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Router\Route;
use Joomla\CMS\Uri\Uri;
use MrVinoth\Component\AllVideoShare\Site\Helper\AllVideoShareHelper;
use MrVinoth\Component\AllVideoShare\Site\Helper\AllVideoShareRoute;

HTMLHelper::_( 'bootstrap.tooltip' );

$app = Factory::getApplication();

// Vars
$columns = (int) $this->params->get( 'cols', 3 );
$column = 0;
$column_no = floor( 12 / $columns );

$column_class = 'col-sm-' . $column_no;

$image_ratio  = $this->params->get( 'image_ratio', 56.25 );
$player_ratio = $this->params->get( 'player_ratio', 56.25 );

$popup_class  = $this->params->get( 'popup' ) ? ' avs-popup' : '';

// Import CSS
$wa = $app->getDocument()->getWebAssetManager();

if ( $this->params->get( 'popup' ) ) {
	$wa->useStyle( 'com_allvideoshare.popup' )
		->useScript( 'com_allvideoshare.popup' )
		->useScript( 'com_allvideoshare.site' );
}

$wa->useStyle( 'com_allvideoshare.site' );

if ( $css = $this->params->get( 'custom_css' ) ) {
    $wa->addInlineStyle( $css );
}
?>

<div id="avs-category" class="avs category">
	<?php if ( $this->params->get( 'show_page_heading' ) ) : ?>
		<div class="page-header">
			<h1>
				<?php if ( $this->escape( $this->params->get( 'page_heading' ) ) ) : ?>
					<?php echo $this->escape( $this->params->get( 'page_heading' ) ); ?>
				<?php else : ?>
					<?php echo $this->escape($this->params->get( 'page_title' ) ); ?>
				<?php endif; ?>
			</h1>
		</div>
	<?php endif; ?>

	<?php if ( empty( $this->items ) && empty( $this->subCategories ) ) : ?>
		<div class="alert alert-info">
			<?php echo Text::_( 'COM_ALLVIDEOSHARE_NO_ITEMS_FOUND' ); ?>
		</div>
	<?php else : ?>
		<div class="avs-grid<?php echo $popup_class; ?> mb-4" data-player_ratio="<?php echo (float) $player_ratio; ?>">
			<div class="row">
				<?php foreach ( $this->items as $i => $item ) : 
					if ( $column >= $columns ) {
						echo '</div><div class="row">';
						$column = 0;
					}

					$route = AllVideoShareRoute::getVideoRoute( $item->slug );
					$item_link = Route::_( $route );

					if ( $this->params->get( 'popup' ) ) {
						$item_link = 'javascript:void(0)';
					}			

					$iframe_src = URI::root() . 'index.php?option=com_allvideoshare&view=player&vid=' . $item->id . "&format=raw&autoplay=1";
					?>
					<div id="brown" class="avs-grid-item avs-video-<?php echo (int) $item->id; ?> <?php echo $column_class; ?>" data-mfp-src="<?php echo $iframe_src; ?>">
						<div id="brown" class="card mb-3">
							<a href="<?php echo $item_link; ?>" class="avs-responsive-item" style="padding-bottom: <?php echo (float) $image_ratio; ?>%">
								<div class="avs-image" style="background-image: url('<?php echo AllVideoShareHelper::getImage( $item ); ?>');">&nbsp;</div>
							
								<svg class="avs-svg-icon avs-svg-icon-play"width="78" height="78" viewBox="0 0 78 78">
                                  <!--
									<path d="M16 0c-8.837 0-16 7.163-16 16s7.163 16 16 16 16-7.163 16-16-7.163-16-16-16zM16 29c-7.18 0-13-5.82-13-13s5.82-13 13-13 13 5.82 13 13-5.82 13-13 13zM12 9l12 7-12 7z"></path>    
                  -->
                               <path d="M39,75A36,36,0,1,1,75,39,36,36,0,0,1,39,75ZM28.41,21.68c-2.31,0-4.64,1.72-4.64,5.54V53.79c0,3.31,1.87,5.54,4.64,5.54a6.54,6.54,0,0,0,3.26-1L54.89,45.05c2-1.14,3.1-2.76,3.1-4.54s-1.1-3.4-3.1-4.55L31.67,22.65A6.56,6.56,0,0,0,28.41,21.68Z" fill="#ffc0c0"/><path d="M39,4.5A34.5,34.5,0,1,1,4.5,39,34.27,34.27,0,0,1,39,4.5M28.41,60.83a8,8,0,0,0,4-1.16L55.64,46.35c2.45-1.4,3.85-3.53,3.85-5.84a6.86,6.86,0,0,0-3.85-5.85L32.42,21.35a8.05,8.05,0,0,0-4-1.17c-3.61,0-6.14,2.9-6.14,7V53.79c0,4.14,2.53,7,6.14,7M39,1.5A37.5,37.5,0,1,0,76.5,39,37.5,37.5,0,0,0,39,1.5ZM28.41,57.83c-1.86,0-3.14-1.48-3.14-4V27.22c0-2.56,1.28-4,3.14-4a5.15,5.15,0,0,1,2.52.77L54.15,37.26c3.12,1.79,3.12,4.7,0,6.49L30.93,57.07a5.21,5.21,0,0,1-2.52.76Z" fill="#311e0d"/>
								</svg>
							</a>

							<div id="brown" class="card-body">
								<div class="aiovg-title">
									<a href="<?php echo $item_link; ?>" class="card-link"><?php echo $this->escape( $item->title ); ?></a>
								</div>

								<?php if ( $this->params->get( 'views' ) ) : ?>
									<div class="avs-views-count">
										<?php
										Text::sprintf(
											'COM_ALLVIDEOSHARE_VIDEOS_VIEWS_COUNT', 
											$item->views
										);
										?>
									</div>
								<?php endif; ?>
							</div>					
						</div>
					</div>
					<?php
					if ( $column >= $columns ) echo '</div>';
					$column++;
				endforeach; ?>
			</div>

			<?php echo $this->pagination->getListFooter(); ?>
		</div>		

		<?php if ( ! empty( $this->subCategories ) ) : 
			$column = 0;
			?>

			<?php if ( ! empty( $this->items ) ) : ?>
				<p class="lead"><?php echo Text::_( 'COM_ALLVIDEOSHARE_TITLE_SUBCATEGORIES' ); ?></p>
			<?php endif; ?>

			<div class="avs-grid">
				<div class="row">
					<?php foreach ( $this->subCategories as $i => $item ) : 
						if ( $column >= $columns ) {
							echo '</div><div class="row">';
							$column = 0;
						}

						$route = AllVideoShareRoute::getCategoryRoute( $item->slug );
						$item_link = Route::_( $route );
						?>
						<div class="<?php echo $column_class; ?>">
							<div class="card mb-3">
								<a href="<?php echo $item_link; ?>" class="avs-responsive-item" style="padding-bottom: <?php echo $image_ratio; ?>%">
								<div class="avs-image" style="background-image: url('<?php echo AllVideoShareHelper::getImage( $item ); ?>');">&nbsp;</div>
								</a>

								<div class="card-body">
									<a href="<?php echo $item_link; ?>" class="card-link"><?php echo $this->escape( $item->name ); ?></a>
								</div>					
							</div>
						</div>
						<?php
						if ( $column >= $columns ) echo '</div>';
						$column++;
					endforeach; ?>
				</div>
			</div>
		<?php endif; ?>
	<?php endif; ?>
</div>