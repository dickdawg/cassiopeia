<?php
/**
* @title		Minitek Wall
* @copyright   	Copyright (C) 2011-2021 Minitek, All rights reserved.
* @license   	GNU General Public License version 3 or later.
* @author url   https://www.minitek.gr/
* @developers   Minitek.gr
*/

defined('_JEXEC') or die;

if (!empty($this->wall) ||  $this->wall!== 0)
{
	$options = $this->getColumnsItemOptions();

	foreach ($this->wall as $key=>$item)
	{
		// Cat Filters
		$catfilter = '';
		
		if (isset($item->itemCategoriesRaw) && $item->itemCategoriesRaw)
		{
			foreach($item->itemCategoriesRaw as $category)
			{
				if (is_array($category) && isset($category['title']))
				{
					$catfilter .= ' cat-'.$this->utilities->cleanName($category['title']);
				}
			}
		}

		// Tag Filters
		$tagfilter = '';

		if (isset($item->itemTags))
		{
			foreach($item->itemTags as $tag_name)
			{
				$tagfilter .= ' tag-'.$this->utilities->cleanName($tag_name->title);
			}
		}

		// Date Filters
		$datefilter = '';

		if (isset($item->itemDateRaw))
		{
			$datefilter .= ' date-'.\JHTML::_('date', $item->itemDateRaw, 'Y-m');
		}
		
		?><div class="mwall-item <?php 
			echo $catfilter; ?> <?php 
			echo $tagfilter; ?> <?php 
			echo $datefilter; ?> <?php 
			echo $this->hoverEffectClass; ?>" style="padding:<?php 
			echo (int)$this->gutter; ?>px;" <?php 
			if (isset($item->itemID)) 
			{
				?>data-id="<?php echo (int)$item->itemID; ?>" <?php 
			}
			?>data-title="<?php echo strtolower(htmlspecialchars($item->itemTitleRaw)); ?>" <?php 
			if (isset($item->itemDateRaw)) 
			{
				?>data-date="<?php echo $item->itemDateRaw; ?>" <?php 
			}
			if (isset($item->itemHits)) 
			{
				?>data-hits="<?php echo (int)$item->itemHits; ?>" <?php 
			}
			if (isset($item->itemOrdering)) 
			{
				?>data-ordering="<?php echo (int)$item->itemOrdering; ?>" <?php 
			}
			if (isset($item->itemFOrdering)) 
			{
				?>data-fordering="<?php echo (int)$item->itemFOrdering; ?>" <?php 
			}
			if (isset($item->itemAlias)) 
			{
				?>data-alias="<?php echo $item->itemAlias; ?>" <?php 
			}
			if (isset($item->itemModified)) 
			{
				?>data-modified="<?php echo $item->itemModified; ?>" <?php 
			}
			if (isset($item->itemStart)) 
			{
				?>data-start="<?php echo $item->itemStart; ?>" <?php 
			}
			if (isset($item->itemFinish)) 
			{
				?>data-finish="<?php echo $item->itemFinish; ?>" <?php 
			}
			if (isset($item->rawIndex)) 
			{
				?>data-index="<?php echo $item->rawIndex; ?>"<?php 
			}
			?>><?php

			?><div class="mwall-item-outer-cont <?php 
				if ($this->detailBoxColumns && (isset($item->itemImage) && $item->itemImage && $this->mas_images))
				{
					echo $options['position_class'];
				}
				?>" style="<?php 
					if ($this->mas_border_radius) 
					{
						?>border-radius: <?php echo $this->mas_border_radius; ?>px; <?php 
					}
					if ($this->mas_border) 
					{
						?>border: <?php echo $this->mas_border; ?>px solid <?php echo $this->mas_border_color; ?>; <?php 
					}
					echo $this->animated_flip; 
				?>"><?php 

				?><div class="mwall-item-inner-cont" style="background-color: rgba(<?php echo $options['db_bg_class']; ?>,<?php echo $options['db_bg_opacity_class']; ?>);"><?php 
				
					if (isset($item->itemImage) && $item->itemImage && $this->mas_images) 
					{
						?><div class="mwall-cover <?php echo $this->hoverEffectClass; ?>">
							<div class="mwall-img-div" style=" <?php echo $this->animated_flip; ?>">
								<div class="mwall-item-img"><?php 
									if (isset($item->itemLink) && $this->mas_image_link) 
									{
										?><a href="<?php echo $item->itemLink; ?>" class="mwall-photo-link">
											<img src="<?php echo $item->itemImage; ?>" alt="<?php echo htmlspecialchars($item->itemTitleRaw); ?>" />
										</a><?php 
									} 
									else 
									{
										?><div
											class="mwall-photo-link">
											<img src="<?php echo $item->itemImage; ?>" alt="<?php echo htmlspecialchars($item->itemTitleRaw); ?>" />
										</div><?php 
									}
								?></div><?php 
									
								if ($this->hoverBox) 
								{
									?><div class="mwall-hover-box" style="<?php 
										echo $this->animated; 
										?>background-color: rgba(<?php echo $this->hb_bg_class; ?>,<?php echo $this->hb_bg_opacity_class; ?>);"><?php 

										?><div class="mwall-hover-box-content <?php echo $this->hoverTextColor; ?>"><?php 
										
											if ($this->hoverBoxDate && isset($item->itemDate)) 
											{
												?><div class="mwall-date"><?php 
													echo $item->itemDate; 
												?></div><?php 
											}

											if ($this->hoverBoxTitle) 
											{
												?><h3 class="mwall-title"><?php 
													if (isset($item->itemLink) && $this->detailBoxTitleLink) 
													{
														?><a href="<?php echo $item->itemLink; ?>"><?php 
															echo $item->hover_itemTitle; 
														?></a><?php 
													} 
													else 
													{
														?><span><?php 
															echo $item->hover_itemTitle; 
														?></span><?php 
													}
												?></h3><?php 
											}

											if ($this->hoverBoxCategory || $this->hoverBoxAuthor) 
											{
												?><div class="mwall-item-info"><?php 
												
													if (isset($item->itemCategoriesRaw) && $item->itemCategoriesRaw && $this->hoverBoxCategory) 
													{
														?><p class="mwall-item-category">
															<span class="badge bg-dark"><?php echo \JText::sprintf('COM_MINITEKWALL_IN_CATEGORIES', $item->itemCategory); ?></span><?php 
														?></p><?php 
													}

													if (isset($item->itemAuthorRaw) && $item->itemAuthorRaw && $this->hoverBoxAuthor) 
													{
														?><p class="mwall-item-author">
															<span><?php echo \JText::sprintf('COM_MINITEKWALL_BY_AUTHOR', $item->itemAuthor); ?> </span><?php 
														?></p><?php 
													}

												?></div><?php 
											}

											if ($this->hoverBoxIntrotext) 
											{
												if (isset($item->hover_itemIntrotext) && $item->hover_itemIntrotext) 
												{
													?><div class="mwall-desc"><?php 
														echo $item->hover_itemIntrotext; ?>
													</div><?php 
												}
											}

											if ($this->hoverBoxHits && isset($item->itemHits)) 
											{
												?><div class="mwall-hits">
													<span><?php echo $item->itemHits; ?>&nbsp;<?php echo \JText::_('COM_MINITEKWALL_HITS'); ?><svg xmlns="http://www.w3.org/2000/svg" width="25" height="16.81" viewBox="0 0 25 16.81"><path fill="#555" d="M11.48,15.74A9.31,9.31,0,0,1,7,15.33a10.22,10.22,0,0,1-3.46-2.09,8.14,8.14,0,0,1-3-7,2.17,2.17,0,0,0-.4-1.63C-.06,4.33,0,4.2.16,4L1.71,2.1a.42.42,0,0,1,.46-.2c.2.05.26.21.29.39.15.83.3,1.67.47,2.5a.84.84,0,0,0,.17.4c.58.74,1.15,1.49,1.78,2.19A8.45,8.45,0,0,0,7.07,9.22a5.16,5.16,0,0,0,3.43.55c-.18-.7-.14-1.33.6-1.65s1.23.05,1.59.69a11.68,11.68,0,0,0,2.46-2.24c.87-1,1.73-2.1,2.61-3.13a6.39,6.39,0,0,1,3.62-2.2A2,2,0,0,0,22.71.46a1.26,1.26,0,0,1,1.7-.23,1.29,1.29,0,0,1,.44,1.68c-.14.25-.35.46-.51.7a.66.66,0,0,0-.13.37A6.8,6.8,0,0,1,22.63,7.6c-.72.88-1.44,1.75-2.17,2.61a16.06,16.06,0,0,1-3.89,3.42c-.83.51-1.7.94-2.53,1.4a5.27,5.27,0,0,1,0,1,1,1,0,0,1-.8.69c-.68.17-1.2-.19-1.53-1.06Zm6.29-3.91A16.42,16.42,0,0,0,20,9.58l2-2.42A6,6,0,0,0,23.44,2.8a.8.8,0,0,1,.15-.47c.14-.21.33-.4.49-.6a.58.58,0,0,0,0-.84A.58.58,0,0,0,23.2,1a6.1,6.1,0,0,1-.51.61.72.72,0,0,1-.38.21,6.1,6.1,0,0,0-4.15,2.3c-.79,1-1.59,1.92-2.39,2.88a12.48,12.48,0,0,1-2.72,2.46.29.29,0,0,0-.14.36c.29,1.37.57,2.75.85,4.13l.07.27A16.11,16.11,0,0,0,17.77,11.83Zm-7.12-1.36a.4.4,0,0,1-.18.07,5.76,5.76,0,0,1-4.7-1.3A16.49,16.49,0,0,1,2.58,5.76a.38.38,0,0,0-.42-.19c-.27,0-.55,0-.82.06a7.32,7.32,0,0,0,1.58,5.93A9.24,9.24,0,0,0,8.65,15a8.43,8.43,0,0,0,2.92,0Zm.53-1.19q.64,3.2,1.31,6.37a.44.44,0,0,0,.38.39c.32.06.52-.16.45-.49-.43-2.07-.85-4.14-1.28-6.2a1.3,1.3,0,0,0-.22-.46A.4.4,0,0,0,11.2,9,1.33,1.33,0,0,0,11.18,9.28ZM1.85,3.12l-.33.39L1.16,4c-.31.37-.31.37-.06.77a.27.27,0,0,0,.31.17c.24,0,.48,0,.75-.06Z"/><path fill="#555" d="M18.42,5l.57.48a4.82,4.82,0,0,1,2.56-2l-.22-.72A5.38,5.38,0,0,0,18.42,5Z"/><path fill="#555" d="M18.48,6.08l-.55-.45-.71.85.55.46Z"/></svg></span>
												</div><?php 
											}

											if ($this->hoverBoxLinkButton || $this->hoverBoxZoomButton) 
											{
												?><div class="mwall-item-icons"><?php 
													if ($this->hoverBoxLinkButton) 
													{
														if (isset($item->itemLink)) 
														{
															?><a href="<?php echo $item->itemLink; ?>" class="mwall-item-link-icon">
																<i class="fa fa-link"></i>
															</a><?php 
														}
													}

													
													if ($this->hoverBoxZoomButton && (isset($item->itemImage) && $item->itemImage && $this->mas_images)) 
													{
														?><a data-bs-toggle="modal" data-bs-target="#zoomWall_<?php echo $this->widgetID; ?>" class="mwall-zoom mwall-item-zoom-icon" data-src="<?php echo JURI::root().''.$item->itemImageRaw; ?>" data-title="<?php echo htmlspecialchars($item->itemTitleRaw); ?>">
															<i class="fa fa-search"></i>
														</a><?php 												
													}
												?></div><?php 
											}

										?></div>
									</div><?php 
								}

							?></div>
						</div><?php 
					}

					if ($this->detailBoxAll) 
					{
						?><div class="mwall-item-inner mwall-detail-box <?php 
							echo $options['db_color_class']; ?> <?php 
							echo $options['db_class']; ?> <?php 
							echo $options['title_class']; ?> <?php 
							echo $options['introtext_class']; ?> <?php 
							echo $options['date_class']; ?> <?php 
							echo $options['category_class']; ?> <?php 
							echo $options['author_class']; ?> <?php 
							echo $options['tags_class']; ?> <?php 
							echo $options['hits_class']; ?> <?php 
							echo $options['count_class']; ?> <?php 
							echo $options['readmore_class']; ?> <?php 
							if (!isset($item->itemImage) || !$item->itemImage || !$this->mas_images)
							{
								echo 'mwall-no-image';
							} 
							?>"><?php

							if ($this->detailBoxDateAll && isset($item->itemDate)) 
							{
								?><div class="mwall-date"><?php 
									echo $item->itemDate; 
								?></div><?php 
							}

							if ($this->detailBoxTitleAll) 
							{
								?><h3 class="mwall-title"><?php 
									if (isset($item->itemLink) && $this->detailBoxTitleLink) 
									{
										?><a href="<?php echo $item->itemLink; ?>"><?php 
											echo $item->itemTitle; 
										?></a><?php 
									} 
									else 
									{
										?><span><?php 
											echo $item->itemTitle; 
										?></span><?php 
									}
								?></h3><?php 
							}

							
							if (($this->detailBoxCategoryAll && isset($item->itemCategoriesRaw) && $item->itemCategoriesRaw) || 
								($this->detailBoxAuthorAll && isset($item->itemAuthorRaw) && $item->itemAuthorRaw) ||
								($this->detailBoxTagsAll && isset($item->itemTags) && $item->itemTags && isset($item->itemTagsLayout))) 
							{
								?><div class="mwall-item-info"><?php 
								
									if ($this->detailBoxCategoryAll && isset($item->itemCategoriesRaw) && $item->itemCategoriesRaw) 
									{
										?><p class="mwall-item-category">
											<span class="badge bg-dark"><?php echo \JText::sprintf('COM_MINITEKWALL_IN_CATEGORIES', $item->itemCategory); ?></span><?php 
										?></p><?php 
									}

									if ($this->detailBoxAuthorAll && isset($item->itemAuthorRaw) && $item->itemAuthorRaw) 
									{
										?><p class="mwall-item-author">
											<span><?php echo \JText::sprintf('COM_MINITEKWALL_BY_AUTHOR', $item->itemAuthor); ?> </span><?php 
										?></p><?php 
									}

									if ($this->detailBoxTagsAll && isset($item->itemTags) && $item->itemTags && isset($item->itemTagsLayout)) 
									{
										?><div class="mwall-item-tags"><?php
											echo $item->itemTagsLayout;
										?></div><?php
									}

								?></div><?php 
							}

							if ($this->detailBoxIntrotextAll && isset($item->itemIntrotext) && $item->itemIntrotext) 
							{
								?><div class="mwall-desc"><?php 
									echo $item->itemIntrotext;
								?></div><?php 
							}

							
							if ($this->detailBoxHitsAll && isset($item->itemHits)) 
							{
								?><div class="mwall-hits">
									<span><?php echo $item->itemHits; ?>&nbsp;<?php echo \JText::_('COM_MINITEKWALL_HITS'); ?>
  <svg xmlns="http://www.w3.org/2000/svg" width="25" height="16.81" viewBox="0 0 25 16.81"><path fill="#555" d="M11.48,15.74A9.31,9.31,0,0,1,7,15.33a10.22,10.22,0,0,1-3.46-2.09,8.14,8.14,0,0,1-3-7,2.17,2.17,0,0,0-.4-1.63C-.06,4.33,0,4.2.16,4L1.71,2.1a.42.42,0,0,1,.46-.2c.2.05.26.21.29.39.15.83.3,1.67.47,2.5a.84.84,0,0,0,.17.4c.58.74,1.15,1.49,1.78,2.19A8.45,8.45,0,0,0,7.07,9.22a5.16,5.16,0,0,0,3.43.55c-.18-.7-.14-1.33.6-1.65s1.23.05,1.59.69a11.68,11.68,0,0,0,2.46-2.24c.87-1,1.73-2.1,2.61-3.13a6.39,6.39,0,0,1,3.62-2.2A2,2,0,0,0,22.71.46a1.26,1.26,0,0,1,1.7-.23,1.29,1.29,0,0,1,.44,1.68c-.14.25-.35.46-.51.7a.66.66,0,0,0-.13.37A6.8,6.8,0,0,1,22.63,7.6c-.72.88-1.44,1.75-2.17,2.61a16.06,16.06,0,0,1-3.89,3.42c-.83.51-1.7.94-2.53,1.4a5.27,5.27,0,0,1,0,1,1,1,0,0,1-.8.69c-.68.17-1.2-.19-1.53-1.06Zm6.29-3.91A16.42,16.42,0,0,0,20,9.58l2-2.42A6,6,0,0,0,23.44,2.8a.8.8,0,0,1,.15-.47c.14-.21.33-.4.49-.6a.58.58,0,0,0,0-.84A.58.58,0,0,0,23.2,1a6.1,6.1,0,0,1-.51.61.72.72,0,0,1-.38.21,6.1,6.1,0,0,0-4.15,2.3c-.79,1-1.59,1.92-2.39,2.88a12.48,12.48,0,0,1-2.72,2.46.29.29,0,0,0-.14.36c.29,1.37.57,2.75.85,4.13l.07.27A16.11,16.11,0,0,0,17.77,11.83Zm-7.12-1.36a.4.4,0,0,1-.18.07,5.76,5.76,0,0,1-4.7-1.3A16.49,16.49,0,0,1,2.58,5.76a.38.38,0,0,0-.42-.19c-.27,0-.55,0-.82.06a7.32,7.32,0,0,0,1.58,5.93A9.24,9.24,0,0,0,8.65,15a8.43,8.43,0,0,0,2.92,0Zm.53-1.19q.64,3.2,1.31,6.37a.44.44,0,0,0,.38.39c.32.06.52-.16.45-.49-.43-2.07-.85-4.14-1.28-6.2a1.3,1.3,0,0,0-.22-.46A.4.4,0,0,0,11.2,9,1.33,1.33,0,0,0,11.18,9.28ZM1.85,3.12l-.33.39L1.16,4c-.31.37-.31.37-.06.77a.27.27,0,0,0,.31.17c.24,0,.48,0,.75-.06Z"/><path fill="#555"  d="M18.42,5l.57.48a4.82,4.82,0,0,1,2.56-2l-.22-.72A5.38,5.38,0,0,0,18.42,5Z"/><path fill="#555"  d="M18.48,6.08l-.55-.45-.71.85.55.46Z"/></svg>
  </span>
								</div><?php 
							}

							
							if ($this->detailBoxCountAll && isset($item->itemCount)) 
							{
								?><div class="mwall-count">
									<p><?php echo $item->itemCount; ?></p>
								</div><?php 
							}

							
							if ($this->detailBoxReadmoreAll) 
							{
								if (isset($item->itemLink)) 
								{
									?><div class="mwall-readmore">
										<a href="<?php echo $item->itemLink; ?>"><?php echo \JText::_('COM_MINITEKWALL_READ_MORE'); ?></a>
									</div><?php 
								}
							}

						?></div><?php 
					}

					if (! $this->mas_images || !isset($item->itemImage) || !$item->itemImage) 
					{
						if ($this->hoverBox) 
						{
							?><div class="mwall-hover-box" style="<?php 
								echo $this->animated; 
								?> background-color: rgba(<?php echo $this->hb_bg_class; ?>,<?php echo $this->hb_bg_opacity_class; ?>);"><?php 

								?><div class="mwall-hover-box-content <?php echo $this->hoverTextColor; ?>"><?php 
								
									if ($this->hoverBoxDate && isset($item->itemDate)) 
									{
										?><div class="mwall-date"><?php 
											echo $item->itemDate; 
										?></div><?php 
									}

									if ($this->hoverBoxTitle) 
									{
										?><h3 class="mwall-title"><?php 
											if (isset($item->itemLink) && $this->detailBoxTitleLink) 
											{
												?><a href="<?php echo $item->itemLink; ?>"><?php 
													echo $item->hover_itemTitle; 
												?></a><?php 
											} 
											else 
											{
												?><span><?php 
													echo $item->hover_itemTitle; 
												?></span><?php 
											}
										?></h3><?php 
									}

									if ($this->hoverBoxCategory || $this->hoverBoxAuthor) 
									{
										?><div class="mwall-item-info"><?php 
										
											if (isset($item->itemCategoriesRaw) && $item->itemCategoriesRaw && $this->hoverBoxCategory) 
											{
												?><p class="mwall-item-category">
													<span class="badge bg-dark"><?php 
                                                      ?></span></p><?php 
											}

											if (isset($item->itemAuthorRaw) && $item->itemAuthorRaw && $this->hoverBoxAuthor) 
											{
												?><p class="mwall-item-author">
													<span><?php echo \JText::sprintf('COM_MINITEKWALL_BY_AUTHOR', $item->itemAuthor); ?> </span><?php 
												?></p><?php 
											}

										?></div><?php 
									}

									if ($this->hoverBoxIntrotext) 
									{
										if (isset($item->hover_itemIntrotext) && $item->hover_itemIntrotext) 
										{
											?><div class="mwall-desc"><?php 
												echo $item->hover_itemIntrotext; 
											?></div><?php 
										}
									}

									if ($this->hoverBoxHits && isset($item->itemHits)) 
									{
										?><div class="mwall-hits">
											<p><?php echo $item->itemHits; ?>&nbsp;<?php echo \JText::_('COM_MINITEKWALL_HITS'); ?></p>
										</div><?php 
									}

									if ($this->hoverBoxLinkButton || $this->hoverBoxZoomButton) 
									{
										?><div class="mwall-item-icons"><?php 
											if ($this->hoverBoxLinkButton) 
											{
												if (isset($item->itemLink)) 
												{
													?><a href="<?php echo $item->itemLink; ?>" class="mwall-item-link-icon">
														<i class="fa fa-link"></i>
													</a><?php 
												}
											}

											if ($this->hoverBoxZoomButton && (isset($item->itemImage) && $item->itemImage && $this->mas_images)) 
											{
												?><a data-bs-toggle="modal" data-bs-target="#zoomWall_<?php echo $this->widgetID; ?>" class="mwall-zoom mwall-item-zoom-icon" data-src="<?php echo JURI::root().''.$item->itemImageRaw; ?>" data-title="<?php echo htmlspecialchars($item->itemTitleRaw); ?>">
													<i class="fa fa-search"></i>
												</a><?php 	
											}
										?></div><?php 
									}

								?></div>
							</div><?php 
						}

					}

				?></div>
			</div>
		</div><?php 
	}
} 
else 
{
	echo '-'; // =0 // for javascript purposes - Do not remove
}