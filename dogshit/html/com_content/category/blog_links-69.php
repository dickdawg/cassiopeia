<?php
/**
 * @creator     Dog-Shit Network
 * @template    Joomla 4 Dog Shit Template
 * @copyright   (C) 2022 Dog Shit Network <https://www.dog-shit.net>
 */


defined('_JEXEC') or die;

use Joomla\CMS\Router\Route;
use Joomla\Component\Content\Site\Helper\RouteHelper;

?>

<ol class="com-content-blog__links">
	<?php foreach ($this->link_items as $item) : ?>
		<li class="com-content-blog__link">
			<a href="<?php echo Route::_(RouteHelper::getArticleRoute($item->slug, $item->catid, $item->language)); ?>">
				<?php echo $item->title; ?></a>
		</li>
	<?php endforeach; ?>
</ol>
